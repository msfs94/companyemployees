# Projeto

CompanyEmployees

## Descrição

Aplicação desenvolvida para a seleção (2ª fase) do Projeto Samsung 2018.2.

## Execução

```bash

1 - Via prompt comand, acessar o seguinte diretório do projeto enviado:
	"...\companyemployees\src" ;

2.1 - Utilizar o seguinte comando dentro do diretório [passo 1] a fim de 
	compilar o código fonte:
	"javac br/com/companyEmployees/Main.java" ;
	
2.2 - Utilizar o seguinte comando dentro do diretório [passo 1] a fim de 
	executar a aplicação:
	"java br.com.companyEmployees.Main" ;
	
3 - Após a execução correta dos passos anteriores, a aplicação será executada
	e irá exibir a seguinte mensagem : "Enter the comand..." ;

4 - Desta forma, a aplicação estará pronta e aguardando os comandos solicitados 
	na documentação a mim enviada.
	
```

## Desenvolvido por
[Matheus Santana Faustino da Silva](https://www.linkedin.com/in/msfs94/)
package br.com.companyEmployees.config;

public enum ConfigProperties {
	
	PROP_CRITERIA_RESTRICTION_MINCOMPANYTIME (1),
	PROP_CRITERIA_RESTRICTION_TIMEWITHOUTPROGRESSION (2),
	
	PROP_CRITERIA_WEIGHT_COMPANYTIME (2),
	PROP_CRITERIA_WEIGHT_TIMEWITHOUTPROGRESSION (3),
	PROP_CRITERIA_WEIGHT_AGE (5),
	PROP_CRITERIA_WEIGHT_POINTSPERAGE (1);
    
    private final int value;
    
    ConfigProperties(int valueOption){
        value = valueOption;
    }
    public int getValue(){
        return value;
    }

}

package br.com.companyEmployees.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import br.com.companyEmployees.exceptions.general.InvalidListException;
import br.com.companyEmployees.model.Employee;
import br.com.companyEmployees.model.Team;

public class TeamController {
	
	private static Random randon;
	
	public TeamController (){
		randon = new Random();
	}

	public List<Team> allocate(List<Team> teamList, List<Employee> employeeList) throws InvalidListException {

		if (teamList == null) {
			throw new InvalidListException("Team");
		} else if (employeeList == null) {
			throw new InvalidListException("Employee");
		}

		int n;

		if (teamList.size() > 0) {
			int minTeamMaturity = 0;
			for (Team team : teamList) {
				team.setEmployees(new ArrayList<Employee>());
				minTeamMaturity = team.getMinMaturity();

				while (getMaturityTeam(team) < minTeamMaturity && employeeList.size() > 0) {
					n = getNewEmployeeIndex(employeeList);
					team.getEmployees().add(employeeList.get(n));
					employeeList.remove(n);
				}
			}
		}

		return teamList;

	}
	
	public List<Employee> promote(List<Employee> employees, Integer size, Integer currentYear) {
		List<Employee> promotedsEmployees = new ArrayList<Employee>();
		Collections.sort(employees);
		int promoteds = 0;
		for (int i = 0; i < size; i++) {
			if (i < employees.size()) {
				Employee employee = employees.get(i);
				int currentPLevel = employee.getProfessionalLevel();
				int currentLastProgressionYear = employee.getLastProgressionYear();
				if (currentPLevel < 5 && promoteds <= size) {
					employee.setProfessionalLevel(++currentPLevel);
					employee.setLastProgressionYear(++currentLastProgressionYear);
					promotedsEmployees.add(employee);
					promoteds++;
				} else {
					size++;
				}
			}
		}
		return promotedsEmployees;

	}
	
	public List<Employee> getPromotedsEmployees(List<Employee> employees) {
		List<Employee> promoteds = new ArrayList<Employee>();
		for (Employee emp : employees) {
			if (emp.getPoints() > 0) {
				promoteds.add(emp);
			}
		}
		Collections.sort(employees);
		return promoteds;
	}

	public int getMaturityTeam(Team team) {
		int count = 0;
		if (team.getEmployees() != null){
			for (Employee emp : team.getEmployees()) {
				count = count + emp.getProfessionalLevel();
			}
		}
		return count;
		
	}
	
	private int getNewEmployeeIndex(List<Employee> employeeList) {
		int n = randon.nextInt(employeeList.size()) + 0;
		if (employeeList.get(n) == null){
			getNewEmployeeIndex(employeeList);
		}		
		return n;
	}
	
}
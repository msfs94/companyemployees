package br.com.companyEmployees.controller;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import br.com.companyEmployees.config.ConfigProperties;
import br.com.companyEmployees.exceptions.employee.NullOrInvalidEmployee;
import br.com.companyEmployees.exceptions.general.LoadPropertyFileException;
import br.com.companyEmployees.model.Employee;
import br.com.companyEmployees.utilities.PropertiesController;

public class EmployeeController {

	private Properties properties;
	private Integer minCompanyTime, weightCompanyTime, timeMinWithoutProgression, weightTimeWithoutProgression, weightAge, pointsPerAge;

	public EmployeeController() {
		super();
	}

	@SuppressWarnings("unused")
	private void getPropertiesFile() throws LoadPropertyFileException {
		try {
			this.properties = PropertiesController.getProp();
		} catch (IOException e) {
			throw new LoadPropertyFileException();
		}
		
		//criteria
		this.minCompanyTime = Integer.parseInt(properties.getProperty("prop.criteria.restriction.minCompanyTime"));
		this.timeMinWithoutProgression = Integer.parseInt(properties.getProperty("prop.criteria.restriction.timeWithoutProgression"));
		
		//points
		this.weightCompanyTime = Integer.parseInt(properties.getProperty("prop.criteria.weight.companyTime"));
		this.weightTimeWithoutProgression = Integer.parseInt(properties.getProperty("prop.criteria.weight.timeWithoutProgression"));
		this.weightAge = Integer.parseInt(properties.getProperty("prop.criteria.weight.age"));
		this.pointsPerAge = Integer.parseInt(properties.getProperty("prop.criteria.weight.pointsPerAge"));
	}
	
	private void getPropertiesClassProperties(){
		
		//criteria
		this.minCompanyTime = ConfigProperties.PROP_CRITERIA_RESTRICTION_MINCOMPANYTIME.getValue();
		this.timeMinWithoutProgression = ConfigProperties.PROP_CRITERIA_RESTRICTION_TIMEWITHOUTPROGRESSION.getValue();
		
		//points
		this.weightCompanyTime = ConfigProperties.PROP_CRITERIA_WEIGHT_COMPANYTIME.getValue();
		this.weightTimeWithoutProgression = ConfigProperties.PROP_CRITERIA_WEIGHT_TIMEWITHOUTPROGRESSION.getValue();
		this.weightAge = ConfigProperties.PROP_CRITERIA_WEIGHT_AGE.getValue();
		this.pointsPerAge = ConfigProperties.PROP_CRITERIA_WEIGHT_POINTSPERAGE.getValue();
	}

	public Employee setPoints(Employee employee, int currentYear) throws NullOrInvalidEmployee {
		
		//getPropertiesFile();
		getPropertiesClassProperties();

		if (employee != null) {

			Integer points = 0, companyTime = 0, timeWithoutProgression = 0;
			companyTime = currentYear - employee.getAdmissionYear();
			timeWithoutProgression = currentYear - employee.getLastProgressionYear();

			// criteria companyTime
			if (companyTime >= this.minCompanyTime) {
				points = points + (companyTime * this.weightCompanyTime);
			} else {
				employee.setPoints(0);
				return employee;
			}

			// criteria time without progression
			if (employee.getProfessionalLevel() == 4) {
				if (timeWithoutProgression >= timeMinWithoutProgression) {
					points = points + (timeWithoutProgression * this.weightTimeWithoutProgression);
				} else {
					employee.setPoints(0);
					return employee;
				}
			} else {
				points = points + (timeWithoutProgression * this.weightTimeWithoutProgression);
			}

			// criteria age
			int ageEmployee = currentYear - employee.getBirthYear();
			points = points + (((ageEmployee - (ageEmployee % weightAge)) / weightAge) * pointsPerAge);

			employee.setPoints(points);
			
		} else {
			throw new NullOrInvalidEmployee();
		}
		return employee;
	}
	
	public void updateEmployeesPoints(List<Employee> employeeListLoad, Integer currentYear) throws NullOrInvalidEmployee {
		EmployeeController empController = new EmployeeController();
		for (Employee emp : employeeListLoad) {
			emp = empController.setPoints(emp, currentYear);
		}
	}
}

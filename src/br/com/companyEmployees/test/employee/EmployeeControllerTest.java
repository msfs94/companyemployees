package br.com.companyEmployees.test.employee;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.companyEmployees.controller.EmployeeController;
import br.com.companyEmployees.exceptions.employee.NullOrInvalidEmployee;
import br.com.companyEmployees.model.Employee;

public class EmployeeControllerTest {

	EmployeeController empController;
	Employee empLuiz, empLuciano;
	List<Employee> employeeListLoad;
	
	@Before
	public void setUp() throws Exception {
		empLuiz = new Employee();
		empLuciano = new Employee();
		empController = new EmployeeController();
		employeeListLoad = new ArrayList<Employee>();
	}

	@Test
	public void testSetPoints() {
		empLuiz.setName("Luiz");
		empLuiz.setProfessionalLevel(4);
		empLuiz.setBirthYear(1980);
		empLuiz.setAdmissionYear(2005);
		empLuiz.setLastProgressionYear(2014);		
		
		setEmployeePoints();		
		employeeListLoad.add(empLuiz);
		
		assertEquals(45, empLuiz.getPoints(), 0);
	}

	@Test
	public void testUpdateEmployeesPoints() {
		
		empLuiz.setName("Luiz");
		empLuiz.setProfessionalLevel(4);
		empLuiz.setBirthYear(1980);
		empLuiz.setAdmissionYear(2005);
		empLuiz.setLastProgressionYear(2014);		
		employeeListLoad.add(empLuiz);
		
		empLuciano.setName("Luciano");
		empLuciano.setProfessionalLevel(1);
		empLuciano.setBirthYear(1979);
		empLuciano.setAdmissionYear(2011);
		empLuciano.setLastProgressionYear(2013);		
		employeeListLoad.add(empLuciano);
		
		try {
			empController.updateEmployeesPoints(employeeListLoad, 2018);
		} catch (NullOrInvalidEmployee e) {
			fail(e.getMessage());
		}
		
		for (Employee emp : employeeListLoad) {
			if (emp.getPoints() != null) {
				assertFalse(false);
			} else {
				assertFalse(true);
			}
		}
	}

	private void setEmployeePoints() {
		try {
			empController.setPoints(empLuiz, 2018);
		} catch (NullOrInvalidEmployee e) {
			fail(e.getMessage());
		}
	}
}

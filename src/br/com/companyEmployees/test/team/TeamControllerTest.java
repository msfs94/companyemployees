package br.com.companyEmployees.test.team;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.companyEmployees.controller.EmployeeController;
import br.com.companyEmployees.controller.TeamController;
import br.com.companyEmployees.exceptions.employee.NullOrInvalidEmployee;
import br.com.companyEmployees.exceptions.general.InvalidListException;
import br.com.companyEmployees.model.Employee;
import br.com.companyEmployees.model.Team;
import br.com.companyEmployees.utilities.BalanceTeams;
import br.com.companyEmployees.utilities.Tools;

public class TeamControllerTest {

	List<Team> teamListLoad;
	List<Employee> employeeListLoad;
	TeamController teamController;

	@Before
	public void setUp() throws Exception {
		teamController = new TeamController();
		File fileTeamsTest = new File("./properties/teams.txt");
		File fileEmployeesTest = new File("./properties/employees.txt");

		teamListLoad = Tools.readFileTeam(fileTeamsTest.getAbsolutePath());
		employeeListLoad = Tools.readFileEmployee(fileEmployeesTest.getAbsolutePath());
	}

	@Test
	public void testAllocate() {

		List<Team> allocateList = null;
		try {
			allocateList = teamController.allocate(teamListLoad, employeeListLoad);
		} catch (InvalidListException e) {
			fail(e.getMessage());
		}

		for (Team team : allocateList) {
			int currentMaturity = 0;
			int minMaturity = team.getMinMaturity();
			for (Employee emp : team.getEmployees()) {
				currentMaturity = currentMaturity + emp.getProfessionalLevel();
			}
			if (currentMaturity < minMaturity) {
				assertFalse(true);
			} else {
				assertFalse(false);
			}
		}
	}

	@Test
	public void testPromote() {
		EmployeeController empController = new EmployeeController();
		try {
			empController.updateEmployeesPoints(employeeListLoad, 2018);
		} catch (NullOrInvalidEmployee e) {
			fail(e.getMessage());
		}
		
		List<Employee> promotedsEmployees = teamController.promote(employeeListLoad, 2, 2018);
		String testString = "";
		for (Employee employee : promotedsEmployees) {
			testString = testString + " " + employee.getName();
		}
		if (promotedsEmployees.size() == 2 
				&& testString.contains("Luiz") 
				&& testString.contains("Luciano")) {
			assertFalse(false);
		} else {
			assertFalse(true);
		}
	}
	
	@Test
	public void testBalance(){
		EmployeeController empController = new EmployeeController();
		try {
			empController.updateEmployeesPoints(employeeListLoad, 2018);
		} catch (NullOrInvalidEmployee e) {
			fail(e.getMessage());
		}
		BalanceTeams balance = new BalanceTeams();
		List<Team> balancedList = null;
		try {
			balancedList = balance.balance(teamListLoad, employeeListLoad);
		} catch (InvalidListException e) {
			fail(e.getMessage());
		}
		
		for (Team team : balancedList) {
			int maturityTeam = teamController.getMaturityTeam(team);
			int balanceIndex = maturityTeam - team.getMinMaturity();
			if (team.getName().contains("1")){
				if (balanceIndex == 2){
					assertFalse(false);
				} else {
					assertFalse(true);
				}
			}
			if (team.getName().contains("2")){
				if (balanceIndex == 1){
					assertFalse(false);
				} else {
					assertFalse(true);
				}
			}
			if (team.getName().contains("3")){
				if (balanceIndex == 1){
					assertFalse(false);
				} else {
					assertFalse(true);
				}
			}
		}
	}
	

}

package br.com.companyEmployees.facade;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import br.com.companyEmployees.controller.EmployeeController;
import br.com.companyEmployees.controller.TeamController;
import br.com.companyEmployees.exceptions.employee.NullOrInvalidEmployee;
import br.com.companyEmployees.exceptions.general.InvalidListException;
import br.com.companyEmployees.exceptions.general.LoadPropertyFileException;
import br.com.companyEmployees.model.Employee;
import br.com.companyEmployees.model.Team;
import br.com.companyEmployees.utilities.BalanceTeams;
import br.com.companyEmployees.utilities.Tools;

public class Facade {

	public static Facade facade;
	public List<Employee> employeeListLoad;
	public List<Team> teamList;
	private static Scanner scanner;
	private static Integer currentYear = 2018, countProgression = 0;
	private static EmployeeController empController;
	private static TeamController teamController;
	
	public Facade() {
		empController = new EmployeeController();
		teamController = new TeamController();
	}
	
	public static Facade getInstance() {
		if (facade == null) {
			facade = new Facade();
		}
		return facade;
	}	
	
	public List<Team> allocate(List<Team> teamList, List<Employee> employeeList) throws InvalidListException {
		return teamController.allocate(teamList, employeeList);
	}
	
	public int getMaturityTeam(Team team) {
		return teamController.getMaturityTeam(team);
	}

	public String getComandGeneral(String comand) {
		String comandGeneral = null;
		if (comand != null) {
			if (comand.startsWith("load")) {
				comandGeneral = "load";
			} else if (comand.equals("allocate")) {
				comandGeneral = "allocate";
			} else if (comand.startsWith("promote")) {
				comandGeneral = "promote";
			} else if (comand.equals("balance")) {
				comandGeneral = "balance";
			} else if (comand.equals("exit")) {
				comandGeneral = "exit";
			}
		}
		return comandGeneral;
	}

	public String enterComand() {
		System.out.println("Enter the command...");
		
		scanner = new Scanner(System.in);
		String comand = scanner.nextLine();
		return comand;
	}

	public void load(String[] loadAndTwoFiles) throws Exception {
		String teamFile = loadAndTwoFiles[1];
		String employeeFile = loadAndTwoFiles[2];
		
		teamFile = teamFile.replace("/", "\\");
		employeeFile = employeeFile.replace("/", "\\");
		
		teamList = Tools.readFileTeam(teamFile);
		employeeListLoad = Tools.readFileEmployee(employeeFile);
		
		empController.updateEmployeesPoints(employeeListLoad, currentYear+countProgression);
		
		Collections.sort(employeeListLoad);
	}
	
	public List<Employee> promote(List<Employee> employees, Integer size, Integer currentYear) throws NullOrInvalidEmployee, LoadPropertyFileException {
		List<Employee> promote = teamController.promote(employees, size, currentYear);
		updateEmployeesPoints(employeeListLoad, currentYear);
		return promote;
	}
	
	private void updateEmployeesPoints(List<Employee> employeeListLoad, Integer currentYear) throws NullOrInvalidEmployee, LoadPropertyFileException {
		empController.updateEmployeesPoints(employeeListLoad, currentYear);
	}
	
	public List<Team> balance(List<Team> teamList, List<Employee> empList) throws InvalidListException {
		return new BalanceTeams().balance(teamList, empList);
	}

}

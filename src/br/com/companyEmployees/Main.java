package br.com.companyEmployees;

import java.util.ArrayList;
import java.util.List;

import br.com.companyEmployees.exceptions.employee.NullOrInvalidEmployee;
import br.com.companyEmployees.exceptions.general.InvalidListException;
import br.com.companyEmployees.exceptions.general.LoadPropertyFileException;
import br.com.companyEmployees.facade.Facade;
import br.com.companyEmployees.model.Employee;
import br.com.companyEmployees.model.Team;

public class Main {
	
	public static Facade facade;
	private static Integer currentYear = 2018, countProgression = 0;

	public static void main(String[] args) {
		
		facade = Facade.getInstance();

		Boolean continuos = true;
		String msgFinal = "";

		while (continuos) {

			String comand = facade.enterComand();

			String comandGeneral = facade.getComandGeneral(comand);

			if (comandGeneral != null) {

				switch (comandGeneral) {
				case "load":
					String[] pathTwoFiles = comand.split(" ");
					if (pathTwoFiles.length < 3) {
						continuos = true;
						System.out.println("Invalid comand 'load'.");
						break;
					} else {
						try {
							facade.load(pathTwoFiles);
							System.out.println("loaded");
						} catch (Exception e) {
							System.out.println("Sorry, an error occurred while loading file.");
						}
						continuos = true;
						msgFinal = "";
						break;
					}
				case "allocate":
					if (facade.teamList == null) {
						try {
							throw new InvalidListException("Team");
						} catch (InvalidListException e) {
							System.out.println(e.getMessage());
						}
					} else if (facade.employeeListLoad == null) {
						try {
							throw new InvalidListException("Employee");
						} catch (InvalidListException e) {
							System.out.println(e.getMessage());
						}
					} else {
						List<Employee> empList = new ArrayList<>();
						empList.addAll(facade.employeeListLoad);
						List<Team> allocateList = null;
						try {
							allocateList = facade.allocate(facade.teamList, empList);
						} catch (InvalidListException e) { 
							System.out.println(e.getMessage());
						}
						for (Team team : allocateList) {
							System.out.println("\n" + team.getName() + " - Min. Maturity " + team.getMinMaturity()
									+ " - Current Maturity " + facade.getMaturityTeam(team));
							for (Employee emp : team.getEmployees()) {
								System.out.println(emp.getName() + " - " + emp.getProfessionalLevel());
							}
						}
						continuos = true;
						msgFinal = "";
					}

					break;
				case "promote":
					String[] comandSplit = comand.split(" ");
					if (comandSplit.length == 2) {
						int numPromote = 0;

						try {
							numPromote = Integer.parseInt(comandSplit[1]);
						} catch (NumberFormatException e) {
							continuos = true;
							System.out.println("Invalid comand 'promote'.");
							break;
						}

						if (facade.employeeListLoad != null && facade.employeeListLoad.size() > 0) {
							List<Employee> promoteds = null;
							try {
								promoteds = facade.promote(facade.employeeListLoad, numPromote,(currentYear + (++countProgression)));
							} catch (NullOrInvalidEmployee e) {
								System.out.println(e.getMessage());
							} catch (LoadPropertyFileException e) {
								System.out.println(e.getMessage());
							}
							for (Employee employee : promoteds) {
								System.out.println(
										employee.getName() + " - From: " + (employee.getProfessionalLevel() - 1)
												+ " - To: " + employee.getProfessionalLevel());
							}

						} else {
							continuos = true;
							System.out.println("Sorry! Unable to allocate, try uploading files again.");
							break;
						}

					} else {
						continuos = true;
						System.out.println("Invalid comand 'promote'.");
						break;
					}
					break;
				case "balance":
					if (facade.employeeListLoad != null && facade.employeeListLoad.size() > 0 && facade.teamList != null
							&& facade.teamList.size() > 0) {

						List<Team> balanceList = null;
						try {
							balanceList = facade.balance(facade.teamList, facade.employeeListLoad);
						} catch (InvalidListException e) {
							System.out.println(e.getMessage());
						}

						for (Team team : balanceList) {
							System.out.println("\n" + team.getName() 
													+ " - Min. Maturity " + team.getMinMaturity()
													+ " - Current Maturity " + facade.getMaturityTeam(team));
							for (Employee emp : team.getEmployees()) {
								System.out.println(emp.getName() + " - " + emp.getProfessionalLevel());
							}
						}

					} else {
						continuos = true;
						System.out.println("Sorry! Invalid employee or team list. Try loading the files again.");
						break;
					}
					break;
				case "exit":
					continuos = false;
					msgFinal = "";
					break;			
				default:
					continuos = true;
					msgFinal = "";
					break;
				}
			} else {
				continuos = true;
			}

		}

		if (msgFinal != null && !msgFinal.equals("")) {
			System.out.println("Execution completed...");
			System.out.println("Message: " + msgFinal);
		} else {
			System.out.println("Execution completed !");
			if (msgFinal != null && !msgFinal.equals("")) {
				System.out.println("Message: " + msgFinal);
			}
		}

	}

}

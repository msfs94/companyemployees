package br.com.companyEmployees.utilities;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.companyEmployees.exceptions.employee.InvalidLineFileEmployee;
import br.com.companyEmployees.exceptions.general.FileException;
import br.com.companyEmployees.exceptions.team.InvalidLineFileTeam;
import br.com.companyEmployees.model.Employee;
import br.com.companyEmployees.model.Team;

public class Tools {
	
	public static List<Team> readFileTeam(String filePath) throws Exception {
		
		List<Team> teamList = null;
		
		try {
			FileReader file = new FileReader(filePath);

			@SuppressWarnings("resource")
			BufferedReader readFile = new BufferedReader(file);

			String line = readFile.readLine();
			teamList = new ArrayList<Team>();

			int countLine = 0;
			while (line != null) {
				Team team = new Team();

				String[] teamSplit = line.split(",");
				if (teamSplit.length < 2) {
					throw new InvalidLineFileTeam(countLine);
				}

				String teamName = teamSplit[0];
				Integer teamMinMaturity;

				try {
					teamMinMaturity = Integer.parseInt(teamSplit[1]);
				} catch (Exception e) {
					throw new InvalidLineFileTeam(countLine);
				}

				team.setName(teamName);
				team.setMinMaturity(teamMinMaturity);

				teamList.add(team);

				line = readFile.readLine();
				countLine++;
			}

			readFile.close();
			file.close();
		} catch (IOException e) {
			throw new FileException();
		}

		return teamList;
	}
	
	public static List<Employee> readFileEmployee(String filePath) throws Exception {
		List<Employee> employeeList = null;
		try {
			FileReader file = new FileReader(filePath);

			@SuppressWarnings("resource")
			BufferedReader readFile = new BufferedReader(file);
			employeeList = new ArrayList<Employee>();

			String line = readFile.readLine();

			int countLine = 1;
			while (line != null) {
				Employee employee = new Employee();

				String[] employeeSplit = line.split(",");
				if (employeeSplit.length < 5) {
					throw new InvalidLineFileEmployee(countLine);
				}

				String employeeName = employeeSplit[0];
				Integer pLevel, birthYear, admissionYear, lastProgressionYear;

				try {
					pLevel = Integer.parseInt(employeeSplit[1]);
					birthYear = Integer.parseInt(employeeSplit[2]);
					admissionYear = Integer.parseInt(employeeSplit[3]);
					lastProgressionYear = Integer.parseInt(employeeSplit[4]);
				} catch (Exception e) {
					throw new InvalidLineFileEmployee(countLine);
				}

				employee.setName(employeeName);
				employee.setProfessionalLevel(pLevel);				
				employee.setBirthYear(birthYear);
				employee.setAdmissionYear(admissionYear);
				employee.setLastProgressionYear(lastProgressionYear);

				employeeList.add(employee);

				line = readFile.readLine();
				countLine++;
			}

			readFile.close();
			file.close();
		} catch (IOException e) {
			throw new FileException();
		}
		
		return employeeList;
	}

}

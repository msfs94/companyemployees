package br.com.companyEmployees.utilities.comparator;

import java.util.Comparator;

import br.com.companyEmployees.model.Employee;

public class EmployeeComparator implements Comparator<Employee> {

	public int compare(Employee emp1, Employee emp2) {
		if (emp1.getProfessionalLevel() > emp2.getProfessionalLevel()) {
			return -1;
		}
		if (emp1.getProfessionalLevel() < emp2.getProfessionalLevel()) {
			return 1;
		}
		return 0;
	}

}

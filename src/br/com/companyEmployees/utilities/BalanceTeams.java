package br.com.companyEmployees.utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.companyEmployees.exceptions.general.InvalidListException;
import br.com.companyEmployees.model.Employee;
import br.com.companyEmployees.model.Team;
import br.com.companyEmployees.utilities.comparator.EmployeeComparator;

public class BalanceTeams {

	public List<Team> balance(List<Team> teamList, List<Employee> empList) throws InvalidListException {
		if (empList == null || empList.size() <= 0)
			throw new InvalidListException("Employee");

		if (teamList == null || teamList.size() <= 0)
			throw new InvalidListException("Team");

		Collections.sort(empList, new EmployeeComparator());

		cleanEmployees(teamList);
		
		for (Employee emp : empList) {
			empTeam(teamList, emp);
		}

		return teamList;
	}

	private void cleanEmployees(List<Team> teamList) {
		for (Team team : teamList) {
			team.setEmployees(new ArrayList<Employee>());
		}
	}

	private void empTeam(List<Team> teamList, Employee employee) {
		//verifica em qual time o empregado vai ser inserido
		
		Integer index = getIndexCurrentMinMaturityTeams(teamList); //pega o index do time com menor maturidade CORRENTE.
		Team team;
		if (index != null){
			//se o NMT do time for menor que o NMT do empregado, adiciona o emp ao time...
			team = teamList.get(index);
			addEmpTeam(employee, team);
		} else {
			//se todos os times j� tiverem alcan�ado o NM m�nimo...
			//ser� inserido o empregado no time que tiver a menor diferen�a entre o NM m�n e o corrente.
			index = getIndexCurrentBalanceMinMaturityTeams(teamList);
			team = teamList.get(index);
			addEmpTeam(employee, team);
		}
	}

	private void addEmpTeam(Employee emp, Team team) {
		//adiciona o empregado a lista de empregados do time
		if (team.getEmployees() == null) {
			team.setEmployees(new ArrayList<Employee>());
			team.getEmployees().add(emp);
		} else {
			team.getEmployees().add(emp);
		}
	}

	private Integer getIndexCurrentMinMaturityTeams(List<Team> teamList) {
		//busca o time com o menor n�vel de maturidade corrente...
		//RETORNA NULL SE N�O ENCONTRAR UM TIME COM A MATURIDADE ATUAL MENOR QUE MATURIDADE M�NIMA
		Integer indexMinMaturityTeam = null, minMaturityTeam = 99999;
		int countMaturityEmployees = 0, iTeam = 0;
		for (Team team : teamList) {
			countMaturityEmployees = 0;
			for(Employee emp : team.getEmployees()){
				countMaturityEmployees = countMaturityEmployees + emp.getProfessionalLevel();	
			}
			if (minMaturityTeam > countMaturityEmployees && currentMinMaturityTeam(team) != null){
				minMaturityTeam = countMaturityEmployees;
				indexMinMaturityTeam = iTeam;
			}
			iTeam++;
		}
		
		return indexMinMaturityTeam;
	}
	
	private Integer getIndexCurrentBalanceMinMaturityTeams(List<Team> teamList) {
		int iMinMaturityTeam = 0, minDiferenceMaturityTeam = 9999;
		int countMaturityEmployees = 0, iTeam = 0;
		for (Team team : teamList) {
			countMaturityEmployees = 0;
			for(Employee emp : team.getEmployees()){
				countMaturityEmployees = countMaturityEmployees + emp.getProfessionalLevel();	
			}
			
			//considerando que o time j� esta com o niv. de mat. m�n. atingido...
			//obt�m a diferen�a entre o niv. de mat. atual - niv. de mat. m�n. 
			int diferenceBalanceMinMaturity = Math.abs(countMaturityEmployees - team.getMinMaturity());
			
			if (diferenceBalanceMinMaturity < minDiferenceMaturityTeam ){
				minDiferenceMaturityTeam = diferenceBalanceMinMaturity;
				iMinMaturityTeam = iTeam;
			}
			iTeam++;
		}
		
		return iMinMaturityTeam;
	}
	
	private Integer currentMinMaturityTeam(Team team) {
		int countMaturityEmployees = 0;
		for (Employee emp : team.getEmployees()) {
			countMaturityEmployees = countMaturityEmployees + emp.getProfessionalLevel();
		}
		if (countMaturityEmployees >= team.getMinMaturity()) {
			return null;
		}
		return countMaturityEmployees;
	}
}

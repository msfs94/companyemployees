package br.com.companyEmployees.exceptions.general;

public class LoadPropertyFileException extends Exception {

	private static final long serialVersionUID = 8583740109750081232L;

	public LoadPropertyFileException() {
		super("Error loading property file.");
	}

}

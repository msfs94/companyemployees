package br.com.companyEmployees.exceptions.general;

public class FileException extends Exception {

	private static final long serialVersionUID = 8583740109750081232L;

	public FileException() {
		super("Invalid line file team.");
	}

}

package br.com.companyEmployees.exceptions.general;

public class InvalidListException extends Exception {

	private static final long serialVersionUID = 8583740109750081232L;

	public InvalidListException(String msg) {
		super("Invalid " + msg + " list.");
	}

}

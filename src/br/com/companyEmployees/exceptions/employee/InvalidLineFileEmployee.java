package br.com.companyEmployees.exceptions.employee;

public class InvalidLineFileEmployee extends Exception {

	private static final long serialVersionUID = 8583740109750081232L;

	public InvalidLineFileEmployee(Integer lineError) {
		super("Invalid line file employee. \n Line erro: " + lineError);
	}

}

package br.com.companyEmployees.exceptions.employee;

public class NullOrInvalidEmployee extends Exception {

	private static final long serialVersionUID = 8583740109750081232L;

	public NullOrInvalidEmployee() {
		super("Null or invalid employee!");
	}

}

package br.com.companyEmployees.exceptions.team;

public class InvalidLineFileTeam extends Exception {

	private static final long serialVersionUID = 8583740109750081232L;

	public InvalidLineFileTeam(Integer lineError) {
		super("Invalid line file team. \n Line erro: " + lineError);
	}

}

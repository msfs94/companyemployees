package br.com.companyEmployees.model;

import java.util.List;

public class Company {

	private String name;
	private List<Team> clients;

	public List<Team> getClients() {
		return clients;
	}

	public void setClients(List<Team> clients) {
		this.clients = clients;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

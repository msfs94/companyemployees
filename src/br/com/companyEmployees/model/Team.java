package br.com.companyEmployees.model;

import java.util.List;

public class Team {

	private String name;
	private Integer minMaturity;
	private List<Employee> employees;

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getMinMaturity() {
		return minMaturity;
	}

	public void setMinMaturity(Integer minMaturity) {
		this.minMaturity = minMaturity;
	}

	@Override
	public String toString() {
		return "Team [name=" + name + ", minMaturity=" + minMaturity + ", employees=" + employees + "]";
	}
}

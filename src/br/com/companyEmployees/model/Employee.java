package br.com.companyEmployees.model;

public class Employee implements Comparable<Employee> {

	private String name;
	private Integer professionalLevel;
	private Integer birthYear;
	private Integer admissionYear;
	private Integer lastProgressionYear;
	private Integer points;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getProfessionalLevel() {
		return professionalLevel;
	}

	public void setProfessionalLevel(Integer professionalLevel) {
		this.professionalLevel = professionalLevel;
	}

	public Integer getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(Integer birthYear) {
		this.birthYear = birthYear;
	}

	public Integer getAdmissionYear() {
		return admissionYear;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public void setAdmissionYear(Integer admissionYear) {
		this.admissionYear = admissionYear;
	}

	public Integer getLastProgressionYear() {
		if (lastProgressionYear == null) {
			return admissionYear;
		} else {
			return lastProgressionYear;
		}
	}

	public void setLastProgressionYear(Integer lastProgressionYear) {
		this.lastProgressionYear = lastProgressionYear;
	}

	@Override
	public int compareTo(Employee otherEmployee) {
		if (this.getPoints() > otherEmployee.getPoints()) {
			return -1;
		}
		if (this.getPoints() < otherEmployee.getPoints()) {
			return 1;
		}
		return 0;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", professionalLevel=" + professionalLevel + ", pointsToProgression="
				+ points + "]";
	}
	
}
